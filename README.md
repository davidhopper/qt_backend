# Microservices Repo

This repo is the start of our adventure into microservices. The plan is to keep everything
together in this repo until we see a need/improvement for breaking them out separately.


## Vagrant Setup

Vagrant allows you to easily create a prebuilt virtual machine for microservices development.
You'll need vagrant and VirtualBox installed to use the vagrant instance.

Download and install the following applications:

- Vagrant: https://www.vagrantup.com/downloads.html
- VirtualBox: https://www.virtualbox.org/wiki/Downloads

After installing you should also install a vagrant plugin:

- `vagrant install vagrant-vbguest`

This plugin keeps the Virtualbox Guest Additions inside the vm updated so shared folders
do not break on vm kernel updates. Once installed cd to the root of the repo and use
the following vagrant commands:

- `vagrant up` (This will start up and provision the vm. On the first run, the vm will need to download which might take a while)
- `vagrant halt` (stops the vm)
- `vagrant destroy` (deletes the vm -- the base vm is still local to your computer)
- `vagrant ssh` (ssh into the vm)
- `exit` (exit the vm after using `vagrant ssh`)

Each microservice _may_ have its own vm - if it does not or you need help creating one, feel free
to ask on slack. Please see the README of each project for more info on its local setup as not
all projects have included vms.

### VPNs and AWS Credentials

Some microservices require AWS IAM permissions or other credentials to use AWS resources. Because
the vm is NAT'd to your host machine, having an active VPN connection to our AWS VPC will allow
you to access some resources, while others need to have AWS credentials configured. 

To configure AWS credentials for the vagrant box follow these steps:

- `vagrant ssh`
- `nano ~/.aws/credentials`
- replace 'foo' and 'bar' with valid AWS credentials
    - check 1password under 'SES Access / Secret Key'
    - we'll try to automate/improve this process...
- once done editing the file, `ctrl+x` will exit the editor and you'll need to press `y` to save
the changes.


#!/bin/bash


# General Updates
yum -y update
yum -y install yum-utils nano groupinstall development


# Install Python 3.6
yum -y install https://centos7.iuscommunity.org/ius-release.rpm
yum -y install python36u python36u-devel python36u-libs python36u-pip python36u-setuptools python36u-tools


# Install virtualenvwrapper
pip3.6 install virtualenvwrapper


# virtualenvwrapper config
cat <<EOF >> /home/vagrant/.bashrc
# virtualenvwrapper
WORKON_HOME=/home/vagrant/.virtualenvs
VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.6
PROJECT_HOME=/vagrant
source /usr/bin/virtualenvwrapper.sh
EOF


# Install node using nvm
yum -y install gcc-c++ make
su - vagrant -c "curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash"
su - vagrant -c "nvm install node"

# install some "global" npm packages (nvm installs them in a local shim, however they operate correctly)
# we do this for sam local because its fancy scripts break trying to install locally to projects
su - vagrant -c "npm install -g aws-sam-local"


# Install docker
curl -fsSL https://get.docker.com/ | sh
systemctl start docker
systemctl status docker
systemctl enable docker


# Add vagrant user to docker group
gpasswd -a vagrant docker


# Add a dummy aws credentials file
mkdir /home/vagrant/.aws
cat <<EOF > /home/vagrant/.aws/credentials
[default]
aws_access_key_id = bar
aws_secret_access_key = foo
EOF
chown -R vagrant:vagrant /home/vagrant/.aws/


# set up backend
su - vagrant -c "cd /vagrant && npm install"
su - vagrant -c "cd /vagrant && mkvirtualenv backend && pip install -r backend/requirements-dev.txt"

#!/usr/bin/env bash
#
# This script is used to work around an issue assuming a role with serverless/aws-cli
# see https://github.com/serverless/serverless/issues/3833
#

CREDS=$(aws sts assume-role --role-arn arn:aws:iam::973145764612:role/OrganizationAccountAccessRole \
  --role-session-name my-sls-session --out json)
export AWS_ACCESS_KEY_ID=$(echo $CREDS | jq -r '.Credentials.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo $CREDS | jq -r '.Credentials.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo $CREDS | jq -r '.Credentials.SessionToken')

echo $@
$@

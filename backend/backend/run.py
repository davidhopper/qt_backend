import os

from backend import create_app

# default to test since we won't have the env vars from serverless in pytest
config_name = os.environ.get('ENV') or 'test'

# app is the target of serverless-wsgi
app = create_app(config_name)

# this is used to run the flask app without serverless-wsgi (local debugging)
if __name__ == '__main__':
    # disable werkzeug built-in debugger
    app.run(use_debugger=False)

"""
Example REST API using Flask-API
"""

from flask_api import FlaskAPI
from flask_cors import CORS
from backend.blueprints.config import config, app_config
from backend.blueprints.misc_routes import misc_routes

# shop test routes
from backend.blueprints.shop.vin.vin_routes import vin_routes
from backend.blueprints.shop.mpi_routes import mpi_routes
from backend.blueprints.shop.transaction_routes import transaction_routes
from backend.blueprints.shop.auth.shop_auth import shop_auth, jwt
from backend.blueprints.shop.shop_test import shop_test

from backend.db import db


def create_app(config_name):
    app = FlaskAPI(__name__)
    app.config['JWT_SECRET_KEY'] = 'af0626b9-c068-4682-b594-1f2716a3ed07'

    # attach flask-jwt-extended
    jwt.init_app(app)

    # attach flask-cors
    if config_name == 'dev':
        # allow CORS on all routes for all domains
        cors = CORS()
        cors.init_app(app)

    # shop test routes
    app.register_blueprint(vin_routes)
    app.register_blueprint(mpi_routes)
    app.register_blueprint(transaction_routes)
    app.register_blueprint(shop_auth)
    app.register_blueprint(shop_test)

    # update config using config blueprint - i think this may get us into
    # trouble later if other blueprints need access to env vars, but maybe
    # not... we'll see. easy enough to revert if needed. worst case we'll
    # just need to load the config before we register the blueprints
    app.config.from_object(app_config[config_name])

    # can we make this a blueprint? i think so, but i don't think it buys us
    # much since right now we're just using blueprints as a way to modularize
    # resources; if we were using the ORM and broke out models by blueprint
    # it might be worth it...
    db.init_app(app)

    return app

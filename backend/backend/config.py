"""
Environment variables read by this class are populated by serverless using
the config.<env>.yml files.

We might want to use instance folders to hold a config.py, which would
override any configs

http://flask.pocoo.org/docs/0.12/config/#instance-folders
"""
import os

db_uri = "{driver}://{username}:{password}@{host}/{database}".format(
    driver=os.environ.get('DB_DRIVER'),
    username=os.environ.get('DB_USER'),
    password=os.environ.get('DB_PASS'),
    host=os.environ.get('DB_HOST'),
    database=os.environ.get('DB_NAME'),
)


class Config(object):
    # debug hot reloads and adds stack traces/interactive console to errors
    DEBUG = False
    # disable error caching during requests (better errors while testing)
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get("FLASK_SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = db_uri
    # flask-cors
    # @todo soon to be default, change after that (suppresses warn for now)
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI = "sqlite://{}".format(os.environ.get('DB_NAME'))


class Development(Config):
    pass


class StagingConfig(Config):
    pass


class QAConfig(Config):
    pass


class ProductionConfig(Config):
    pass


app_config = {
    'test': TestConfig,
    'dev': Development,
    'qa': QAConfig,
    'stg': StagingConfig,
    'prd': ProductionConfig,
}

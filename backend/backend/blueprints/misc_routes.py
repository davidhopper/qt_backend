from flask import Blueprint
from flask_api import status

misc_routes = Blueprint('misc_routes', __name__)


@misc_routes.route("/")
def home() -> (dict, int):
    message = {"detail": "Welcome to the API"}

    return message, status.HTTP_404_NOT_FOUND


@misc_routes.route("/force500")
def force500() -> (dict, int):
    response = {
        "detail": "Internal Server Error"
    }

    return response, status.HTTP_500_INTERNAL_SERVER_ERROR

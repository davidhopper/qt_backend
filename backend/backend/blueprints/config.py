import os
from flask import Blueprint

config = Blueprint('config', __name__)

db_uri = "{driver}://{username}:{password}@{host}/{database}".format(
    driver=os.environ.get('DB_DRIVER'),
    username=os.environ.get('DB_USER'),
    password=os.environ.get('DB_PASS'),
    host=os.environ.get('DB_HOST'),
    database=os.environ.get('DB_NAME'),
)


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get("FLASK_SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = db_uri
    # @todo soon to be default, change after that (suppresses warn for now)
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    # @todo three slashes should be an in-memory db, but need to verify
    # no tests currently use the db
    SQLALCHEMY_DATABASE_URI = 'sqlite:///vehicles'


class Development(Config):
    TESTING = True
    DEBUG = True


class StagingConfig(Config):
    pass


class QAConfig(Config):
    pass


class ProductionConfig(Config):
    DEBUG = False
    TESTING = False


app_config = {
    'test': TestConfig,
    'dev': Development,
    'qa': QAConfig,
    'stg': StagingConfig,
    'prd': ProductionConfig,
}

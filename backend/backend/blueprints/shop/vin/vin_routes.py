from flask import Blueprint, request, current_app
from flask_api import status, exceptions
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)
import requests
from . import vin_helpers
import xml.etree.cElementTree as et
import json
import time, datetime
from sqlalchemy.sql import text
from backend.db import db

vin_routes = Blueprint('vin_routes', __name__)

#this route takes in a VIN number and decodes it using the AAP decoder
@vin_routes.route("/decode_vin/<vin>", methods=["GET"])
@jwt_required
def vin_decoder(vin) -> dict:

    #verify that this returns the same as PHP code...
    if not vin:
        raise exceptions.ParseError()

    data = vin_helpers._aap_vin_decode(vin)

    currected_vin = ''
    aap_vehicle_code = ''
    aaia_code = ''
    year = ''
    make = ''
    model = ''
    engine_liters = ''
    engine_block_type = ''
    engine_cylinders = ''

    xDoc=et.fromstring(data)

    currected_vin = xDoc.find('Body/VinLookupResponse/CorrectedVIN').text
    year = xDoc.find('Body/VinLookupResponse/Year').text
    make = xDoc.find('Body/VinLookupResponse/Make').text
    model = xDoc.find('Body/VinLookupResponse/Model').text
    engine_liters = xDoc.find('Body/VinLookupResponse/EngineInfoLiters').text
    engine_block_type = xDoc.find('Body/VinLookupResponse/EngineInfoBlockType').text
    engine_cylinders = xDoc.find('Body/VinLookupResponse/EngineInfoCylinders').text

    if year:
        aap_vehicle_code = xDoc.find('Body/VinLookupResponse/AAPVehicleCode').text
        aaia_code = xDoc.find('Body/VinLookupResponse/AAIACode').text

    return {"id": vin, "vin": currected_vin, "year": year, "make": make, "model": model, "engine": engine_liters, "aaiaCode": aaia_code}

#this route takes in a database id of a previous vin scan and returns the data
@vin_routes.route("/vin/scans/<id>", methods=["GET"])
@jwt_required
def get_vin_by_id(id) -> dict:

    #check this return against what happens in PHP  TODO
    if not id:
        raise exceptions.ParseError()
        
    return vin_helpers._scanned_vin_result(id);


@vin_routes.route("/vin/scans", methods=["GET"])
@jwt_required
def get_recent_vins() -> dict:

    claims = get_jwt_claims()
    scid = claims['ServiceCenterId']

    select_sql = """
        SELECT ID, TimeStampUTC, VIN, Year, Make, Model, Engine, AAIACode FROM vinscans 
        WHERE ServiceCenterID=:scid ORDER BY TimeStampUTC DESC LIMIT 100
                 """
    params = {
        'scid': scid
    }

    db_result = db.engine.execute(text(select_sql), params)
    response = []
    for vin_data in db_result:
        isotime = ""
        #need to convert this from php
        #isotime = substr(date('c',strtotime($vin[0]->TimeStampUTC)),0,-6);
        _vin = {
            "id": vin_data['ID'],
            "date": isotime,
            "vin": vin_data['VIN'],
            "year": vin_data['Year'],
            "make": vin_data['Make'],
            "model": vin_data['Model'],
            "engine": vin_data['Engine'],
            "aaiaCode": vin_data['AAIACode']
        }
        response.append(_vin)

    return response

#this route takes in a db id of a vin and vin data and updates it in the db - it then returns the updated data
@vin_routes.route("/vin/scans/<id>", methods=["POST"])
@jwt_required
def update_insert_vin(id) -> dict:

    claims = get_jwt_claims()
    scid = claims['ServiceCenterId']
    empid = claims['EmployeeId']

    # TODO check for valid parameters...
    vin = request.json.get('vin', None)
    #if not VIN  ERROR # test

    year = request.json.get('year')
    make = request.json.get('make')
    model = request.json.get('model')
    engine = request.json.get('engine')
    aaia_code = request.json.get('aaiaCode')

    ts = time.time()
    tsmysql = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    if not id or not year:
        raise exceptions.ParseError()
      
    if id == "0":
        insert_sql = """
           INSERT INTO vinscans (ServiceCenterID, EmployeeID, TimeStampUTC, VIN, Make, 
                                 Model, YEAR, ENGINE, AAIACode) VALUES (
                                  :scid, :empid, :ts, :vin, :make, :model, :year, :engine, :aaiaCode)
                     """
        params = {
            'scid': scid,
            'empid': empid,
            'ts': tsmysql,
            'vin': vin,
            'make': make,
            'model': model,
            'year': year,
            'engine': engine,
            'aaiaCode': aaia_code
        }
        db_result = db.engine.execute(text(insert_sql), params)
        select_sql = """
                        SELECT *
                        FROM vinscans
                        WHERE ServiceCenterID=:scid ORDER BY ID DESC LIMIT 1
                    """
        params = { 'scid': scid }
        vin_result = db.engine.execute(text(select_sql), params).first()
        id = vin_result['ID']
    else:
        upd_sql = "UPDATE vinscans SET TimeStampUTC=:ts WHERE ID=:id"
        params = {
            'ts': tsmysql,
            'id': id
        }
        db_result = db.engine.execute(text(upd_sql), params)

    return vin_helpers._scanned_vin_result(id)


@vin_routes.route("/vin/scans/<vin>", methods=["DELETE"])
@jwt_required
def delete_scanned_vin(vin) -> dict:

    claims = get_jwt_claims()
    scid = claims['ServiceCenterId']

    if not vin:
        raise exceptions.ParseError()
      

    del_statement = """
                        DELETE FROM vinscans WHERE ServiceCenterID=:scid AND VIN=:vin
                    """
    params = {
        'scid': scid,
        'vin': vin
    }
    db_result = db.engine.execute(text(del_statement), params)

    return {"success": "1", "vin": vin}


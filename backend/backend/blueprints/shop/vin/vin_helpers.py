from flask import Blueprint, request, current_app
from flask_api import status, exceptions
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)
import requests
import xml.etree.cElementTree as et
import json
import time, datetime
from sqlalchemy.sql import text
from backend.db import db

def _scanned_vin_result(id):

    select_sql = """
                    SELECT *
                      FROM vinscans
                    WHERE ID=:id
                 """
    params = { 'id': id }
    scanned_vin_result = db.engine.execute(text(select_sql), params).first()

    # TODO time calculation
    isotime = ""
    #need to convert this from php
    #isotime = substr(date('c',strtotime($vin[0]->TimeStampUTC)),0,-6);

    if not scanned_vin_result:
        return {"status": "error", "message": "VIN record not found"}, status.HTTP_404_NOT_FOUND
    else:
        return {"id": scanned_vin_result['ID'], "date": isotime, "vin": scanned_vin_result['VIN'], "year": scanned_vin_result['Year'], "make": scanned_vin_result['Make'], "model": scanned_vin_result['Model'], "engine": scanned_vin_result['Engine'], "aaiaCode": scanned_vin_result['AAIACode']}


def _aap_vin_decode(vin):

    post_data = """
        <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
            <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                    <VinLookupRequest schemaVersion="1.0" xmlns="http://vinlookup.types.service.cwsadapter.ecomm.advancestores.com/">
                        <wsHeader>
                            <wsMessageId xmlns="http://commontypes.types.service.cwsadapter.ecomm.advancestores.com/">404640ea-9430-48f9-99f2-cc14494380e5</wsMessageId>
                        <wsSalesChannel xmlns="http://commontypes.types.service.cwsadapter.ecomm.advancestores.com/">
                            <channeltype>KEYLINK</channeltype>
                        </wsSalesChannel>
                    </wsHeader>
                    <VehicleIdentificationNumber>THE_VIN</VehicleIdentificationNumber>
                    <FullResponse>false</FullResponse>
                </VinLookupRequest>
            </s:Body>
        </s:Envelope>
    """
    post_data = post_data.replace('THE_VIN', vin)

    post_body = {
        'RequestHeader': {
            # this would map to partner_id on our side
            'CompanyId': '1',
            'RequestDateUtc': '',
            'RequestKey': '',
            'RequestingApp': ''
        },
        'RequestBody': post_data
    }

    #move to config file.... TODO
    endpoint = "https://commercial.advanceautoparts.com/b2bservices-KEY/CWSAdapter/VinLookup"

    response = requests.request(
        'POST',
        endpoint,
        data=post_data,
        auth=('', '')
    )

    response.raise_for_status()
    data = response.text

    if not data:
        raise Exception('Could not parse Response from AAP')

    data = data.replace('env:', '')
    data = data.replace('ns2:', '')
    data = data.replace('ns3:', '')
    data = data.replace('ns4:', '')

    return data;

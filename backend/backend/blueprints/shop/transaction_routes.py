from flask import Blueprint, request
from flask_api import status, exceptions
import requests
import xml.etree.cElementTree as et
import json
from sqlalchemy.sql import text
from backend.db import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)

transaction_routes = Blueprint('transaction_routes', __name__)

#this route simpl takes a transaction id and spits out a little information based on the ticket
@transaction_routes.route("/load_ticket/<id>", methods=["GET"])
@jwt_required
def load_ticket(id) -> dict:

    if not id:
        raise exceptions.ParseError()
        
    select_sql = """
                    SELECT *
                      FROM transactions
                    WHERE id=:id
                 """
    params = {
        'id': id
    }

    message = ""    
    result = db.engine.execute(text(select_sql), params).first()
    if not result:
        message = {"detail": "No Records"}
    else:
        message = {"detail": "Last Ticket ID {} Has Customer Name: {} {}".format(result['ID'],result['CustomerFirstName'],result['CustomerLastName'])}

    return message

#just messing around here - this is a save ticket route - will not be needed in QT Phase 1
@transaction_routes.route("/save_ticket/<id>", methods=["POST"])
@jwt_required
def save_ticket(id) -> dict:

    claims = get_jwt_claims()
    scid = claims['ServiceCenterId']
    empid = claims['EmployeeId']

    vin = request.json.get('vin')
    year = request.json.get('year')
    make = request.json.get('make')
    model = request.json.get('model')
    engine = request.json.get('engine')
    aaia_code = request.json.get('aaiaCode')

        #public long ID { get; set; }
        #public long ServiceCenterID { get; set; }
        #public long CompanyID { get; set; }
        #public long RefNo { get; set; }
        #public string Type { get; set; }
        #public System.DateTime CreationDate { get; set; }
        #[PetaPoco.Ignore]
        #public Nullable<System.DateTime> InvoiceDate { get; set; }
        #public Nullable<System.DateTime> LastUpdateDate { get; set; }
        #public Nullable<System.DateTime> ContactDate { get; set; }
        #public Nullable<System.DateTime> InitialContactDate { get; set; }
        #public Nullable<System.DateTime> InShopDate { get; set; }
        #public Nullable<System.DateTime> PromisedByDate { get; set; }
        #public Nullable<System.DateTime> BeginTicketTimestamp { get; set; }
        #public Nullable<System.DateTime> DeliveryDate { get; set; }
        #public Nullable<System.DateTime> AppointmentDate { get; set; }
        #public Nullable<int> AppointmentLengthMinutes { get; set; }
        #public Nullable<int> AppointmentBayNumber { get; set; }
        #public string Status { get; set; }
        #public string PONo { get; set; }
        #public string MarketingLead { get; set; }
        #public Nullable<bool> IsTowIn { get; set; }
        #public Nullable<bool> HasMPI { get; set; }
        #public Nullable<decimal> MPIFee { get; set; }
        #public Nullable<long> InitialContactEmployeeID { get; set; }
        #public Nullable<long> WrittenByEmployeeID { get; set; }
        #public Nullable<long> InspectionID { get; set; }
        #public Nullable<long> FleetAccountID { get; set; }
        #public string FleetAccountPONo { get; set; }
        #public string CWSPromoCode { get; set; }
        #public string CWSPromoDesc { get; set; }
        #public string WarrantyCompany { get; set; }
        #public Nullable<bool> IsWarrantyWork { get; set; }
        #public string WarrantyAddress { get; set; }
        #public string WarrantyCity { get; set; }
        #public string WarrantyState { get; set; }
        #public string WarrantyPhone { get; set; }
        #public string WarrantyFax { get; set; }
        #public string WarrantyAuthByFirstName { get; set; }
        #public string WarrantyAuthByLastName { get; set; }
        #public Nullable<bool> WarrantyBillAtCost { get; set; }
        #public string WarrantyZip { get; set; }
        #public Nullable<long> CustomerID { get; set; }
        #public string CustomerLastName { get; set; }
        #public string CustomerFirstName { get; set; }
        #public string CustomerCompanyName { get; set; }
        #public string CustomerAddress { get; set; }
        #public string CustomerCity { get; set; }
        #public string CustomerState { get; set; }
        #public string CustomerZip { get; set; }
        #public string CustomerCellPhone { get; set; }
        #public string CustomerHomePhone { get; set; }
        #public string CustomerWorkPhone { get; set; }
        #public string CustomerWorkPhoneExt { get; set; }
        #public string CustomerEmail { get; set; }
        #public string OtherAuthPerson { get; set; }
        #public string OtherAuthPhone { get; set; }
        #public Nullable<long> VehicleID { get; set; }
        #public Nullable<int> VehicleYear { get; set; }
        #public string VehicleMake { get; set; }
        #public string VehicleModel { get; set; }
        #public string VehicleEngine { get; set; }
        #public string VehicleAAIAIDs { get; set; }
        #public Nullable<long> VehicleTransmissionID { get; set; }
        #public string VehicleVIN { get; set; }
        #public string VehicleLicenseNo { get; set; }
        #public string VehicleLicenseState { get; set; }
        #public string VehicleColor { get; set; }
        #public string VehicleFleetUnitNo { get; set; }
        #public Nullable<long> VehicleOdometerIn { get; set; }
        #public Nullable<long> VehicleOdometerOut { get; set; }
        #public Nullable<int> VehicleInspectionMonth { get; set; }
        #public string AlertNotes { get; set; }
        #public Nullable<decimal> AuthorizedTotalParts { get; set; }
        #public Nullable<decimal> AuthorizedTotalLabor { get; set; }
        #public Nullable<decimal> AuthorizedSubtotal { get; set; }
        #public Nullable<decimal> TotalParts { get; set; }
        #public Nullable<decimal> TotalCores { get; set; }
        #public Nullable<decimal> TotalLabor { get; set; }
        #public Nullable<decimal> TotalSublet { get; set; }
        #public Nullable<decimal> TotalOther { get; set; }
        #public Nullable<decimal> TotalDiscounts { get; set; }
        #public Nullable<decimal> TotalShopSupplies { get; set; }
        #public Nullable<decimal> TotalHazMat { get; set; }
        #public Nullable<decimal> TaxableSubtotal { get; set; }
        #public Nullable<decimal> NonTaxableSubtotal { get; set; }
        #public Nullable<decimal> Subtotal { get; set; }
        #public Nullable<decimal> SalesTax { get; set; }
        #public Nullable<decimal> GrandTotal { get; set; }
        #public Nullable<decimal> GrandTotalQuoted { get; set; }
        #public Nullable<bool> IsInvoiced { get; set; }
        #public Nullable<bool> IsReconciled { get; set; }
        #public Nullable<bool> IsLegacyImport { get; set; }
        #public string ActivantISEVehicleInfo { get; set; }
        #public Nullable<long> CreditOrigTransactionID { get; set; }
        #public Nullable<long> CreditOrigRefNo { get; set; }
        #public Nullable<long> CreditReasonID { get; set; }
        #public Nullable<long> CreditApprovedByEmployeeID { get; set; }
        #public Nullable<int> VoidEmployeeID { get; set; }
        #public Nullable<System.DateTime> VoidTimeStamp { get; set; }
        #public Nullable<long> InvoicedByEmployeeID { get; set; }
        #public Nullable<DateTime> LastUpdateSync { get; set; }
        #public Nullable<long> SalesTaxProfileID { get; set; }
        #public Nullable<decimal> SalesTaxRate { get; set; }
        #public Nullable<decimal> HazMatRate { get; set; }
        #public Nullable<decimal> ShopSuppliesRate { get; set; }
        #public Nullable<decimal> HazMatMaximum { get; set; }
        #public Nullable<decimal> ShopSuppliesMaximum { get; set; }
        #public Nullable<bool> PosPrefTaxHazMat { get; set; }
        #public Nullable<bool> PosPrefTaxShopSupplies { get; set; }
        #public Nullable<bool> PosPrefApplyShopSuppliesParts { get; set; }
        #public Nullable<bool> PosPrefApplyShopSuppliesLabor { get; set; }
        #public Nullable<bool> PosPrefApplyShopSuppliesMisc { get; set; }

    return ""

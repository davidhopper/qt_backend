from flask import Blueprint, request
from flask_api import status, exceptions
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)
import requests
import xml.etree.cElementTree as et
import json
from sqlalchemy.sql import text
from backend.db import db

shop_test = Blueprint('shop_test', __name__)

# this route simple spits out the current contents of the shop jwt
@shop_test.route("/test_jwt", methods=["GET"])
@jwt_required
def test_jwt() -> dict:

    claims = get_jwt_claims()

    return {"iat": claims['iat'], "exp": claims['exp'], "jti": claims['jti'], "sub": claims['sub'], "scope": claims['scope'], "Useraname": claims['Username'], "CompanyId": claims['CompanyId'], "PasswordHash": claims['PasswordHash'], "ServiceCenterId": claims['ServiceCenterId'], "EmployeeId": claims['EmployeeId'], "IsMobile": claims['IsMobile']}
from flask import Blueprint, request, current_app
from flask_api import exceptions, status
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)
import json
from sqlalchemy.sql import text
from . import shop_auth_helpers
from backend.db import db
from flask import jsonify
import hashlib, binascii

shop_auth = Blueprint('shop_auth', __name__)
jwt = JWTManager()

# Flask JWT Docs:
# https://media.readthedocs.org/pdf/flask-jwt-extended/latest/flask-jwt-extended.pdf
@jwt.user_claims_loader
def add_claims_to_access_token(identity):

    return shop_auth_helpers._get_user_data(identity)

@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    # get claims here
    claims = get_jwt_claims()
    jwt_ph = claims['PasswordHash']

    # get user data from admin db - hard coded for now
    hashed_db_pass = hashlib.sha256(b'H7/YTM8NQgFwJ3pQnJcolAWQ0NPwAxEx3/JevX1KXqht8B4iHLmrDxlXecqzQ43kxLdPjyxizM2NyELFL6UBCnzcdddGjpN34iAyovBHZaBREknmB1qFHDr9rVj8doS0930UY0XsrXnyzIrk5huviqBPDEenzSa+LMDkBtbyMjRNLwAskOHEXHsLkdJQ45tuM4vS9RWZAllRil2xROUtfR+3R0JeAbw3Zz6nXlW9gErBoPaF')
    db_pass_hex_dig = hashed_db_pass.hexdigest()
    current_app.logger.info('db_pass_hex_dig: {}'.format(db_pass_hex_dig))

    if jwt_ph == db_pass_hex_dig:
        return True
    else:
        return None

    # check if user token is still valid
    # if yes, return "True", else return "None"
    return True

# custom unauthorized message
@jwt.user_loader_error_loader
def custom_user_loader_error(identity):
    ret = {
        "msg": "User {} not found".format(identity)
    }

    return jsonify(ret), 401

#this is a route for shop login
@shop_auth.route("/shop_login", methods=["POST"])
def login() -> (dict, int):
    """
    Generate and return a JWT token when given valid credentials
    :return:
    """
    if not request.json:
        raise exceptions.AuthenticationFailed

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username or not password:
       raise exceptions.ParseError

    current_app.logger.info('Attempting login to QA with user {}'.format(username))

    # hard coding this for now - this demo only has a connection to 1 db currently (qa_business)
    # we will need to add code to pull this data from the admin database
    username = "DAVELIVE"
    password = "DAVE5521"
    # Password
    db_pwd = "H7/YTM8NQgFwJ3pQnJcolAWQ0NPwAxEx3/JevX1KXqht8B4iHLmrDxlXecqzQ43kxLdPjyxizM2NyELFL6UBCnzcdddGjpN34iAyovBHZaBREknmB1qFHDr9rVj8doS0930UY0XsrXnyzIrk5huviqBPDEenzSa+LMDkBtbyMjRNLwAskOHEXHsLkdJQ45tuM4vS9RWZAllRil2xROUtfR+3R0JeAbw3Zz6nXlW9gErBoPaF"
    # PasswordKey
    password_key = "5400:Camp8tZ4aGMONeRHr/XfZlWQ6wrmlDagLL91VT/p9T5cuq+patFq3O7Rzu5G1Lwsb0eTeCiSnsVN17PI2y0B"

    current_app.logger.info('About to call verify_hash')
    passwordvalid = shop_auth_helpers._verify_hash(password, db_pwd, password_key)

    # not on failure we are returning 401 here - the PHP code returned a 422 but flask doesn't seem to have that built in:
    # http://www.flaskapi.org/api-guide/status-codes/#client-error-4xx
    if passwordvalid:
        token = create_access_token(identity=username)
        return {"access_token": token}, status.HTTP_200_OK
    else:
        return {"message": "Username or Password Incorrect"}, status.HTTP_401_UNAUTHORIZED




from flask import Blueprint, request, current_app
from flask_api import exceptions, status
from flask_jwt_extended import JWTManager, create_access_token
import json
from sqlalchemy.sql import text
from backend.db import db
import base64
import hashlib, binascii
import hmac
import os
import struct
import random
import time, datetime
from array import array

def generate_random_number(length):
    return int(''.join([str(random.randint(0,10)) for _ in range(length)]))

def _get_user_data(username):

    select_sql = """
            SELECT e.CompanyID, e.ServiceCenterID, e.ID AS EmployeeID
            FROM employees e WHERE e.Username=:username
                 """
    params = { 'username': username }
    user_data = db.engine.execute(text(select_sql), params).first()

    _company_id = user_data['CompanyID']
    _service_center_id = user_data['ServiceCenterID']
    _employee_id = user_data['EmployeeID']
    _rand_16 = generate_random_number(16)
    _jti = base62_encode(_rand_16)

    # hard coded for now - need to grab this from the admin db (this is the password for DAVELIVE)
    hashed_db_pass = hashlib.sha256(b'H7/YTM8NQgFwJ3pQnJcolAWQ0NPwAxEx3/JevX1KXqht8B4iHLmrDxlXecqzQ43kxLdPjyxizM2NyELFL6UBCnzcdddGjpN34iAyovBHZaBREknmB1qFHDr9rVj8doS0930UY0XsrXnyzIrk5huviqBPDEenzSa+LMDkBtbyMjRNLwAskOHEXHsLkdJQ45tuM4vS9RWZAllRil2xROUtfR+3R0JeAbw3Zz6nXlW9gErBoPaF')
    db_pass_hex_dig = hashed_db_pass.hexdigest()
    current_app.logger.info('db_pass_hex_dig: {}'.format(db_pass_hex_dig))

    ts = time.time()
    ts_str = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    nextTime = datetime.datetime.now() + datetime.timedelta(hours = 24)

    # 'identity' is username
    return {
        'iat': ts_str,
        'exp': nextTime.strftime("%Y-%m-%d %H:%M:%S"),
        'jti': _jti,
        'sub': username,
        'scope': [],
        'Username': username,
        'PasswordHash': db_pass_hex_dig,
        'CompanyId': _company_id,
        'ServiceCenterId': _service_center_id,
        'EmployeeId': _employee_id,
        "IsMobile": True
    }

def _get_user(username):

    select_sql = """
            SELECT u.*
            FROM users u WHERE u.Username=:username
                 """
    params = { 'username': username }
    user_data = db.engine.execute(text(select_sql), params).first()

    _company_id = user_data['CompanyID']
    _service_center_id = user_data['ServiceCenterID']
    _employee_id = user_data['EmployeeID']


    # 'identity' is username
    return {
        'Username': username,
        'CompanyId': _company_id,
        'ServiceCenterId': _service_center_id,
        'EmployeeId': _employee_id
    }


def _verify_hash(password_text, password_hash, salt_text):

    #encoded_ph = str.encode(password_hash)

    ret = _compute_hash(password_text, salt_text)
    current_app.logger.info('RET: {}'.format(ret))
    current_app.logger.info('PASSED IN: {}'.format(password_hash))

    ph_array = str.encode(password_hash)
    if ret == bytearray(ph_array):
        return True
    else:
        return False


def _compute_hash(password_text, salt_text): 

    current_app.logger.info('In verify hash')

    _ITERATION_COUNT = 5400;
    _SALT_BYTE_COUNT = 63;
    _OUTPUTHASH_BYTE_COUNT = 180;

    raw_salt = None
    iteration_count = 0
    salt_fields = salt_text.split(":")
    if salt_fields and len(salt_fields) == 2 and salt_fields[0].isnumeric():
        iteration_count = int(salt_fields[0])
        current_app.logger.info('inner if iteration: {}'.format(iteration_count))
        raw_salt_str = salt_fields[1];
        current_app.logger.info('inner if rawSaltStr: {}'.format(raw_salt_str))

        #$rawSalt = base64_decode($saltFields[1]);
        raw_salt = base64.b64decode(raw_salt_str)
        current_app.logger.info('inner if rawSalt decoded: {}'.format(raw_salt))
    else:
        raise exceptions.ParseError()

    pwd_as_bytes = str.encode(password_text)
    #pwd_as_bytes = bytearray(password_text, 'UTF8')
    current_app.logger.info('passwordText Bytes: {}'.format(pwd_as_bytes))

    salted_password_bytes = bytearray(len(pwd_as_bytes) + len(raw_salt));
    i = 0
    while i < len(pwd_as_bytes):
        salted_password_bytes[i] = pwd_as_bytes[i];
        i += 1

    i = 0
    while i < len(raw_salt):
        salted_password_bytes[len(pwd_as_bytes) + i] = raw_salt[i];
        i += 1

    hash_bytes = hashlib.sha512(salted_password_bytes).digest()
    if not hash_bytes:
        raise exceptions.ParseError()

    #now convert hashBytes to base 64 string
    #hash_bytes = base64.b64encode(hash_bytes)
    current_app.logger.info('hash_bytes: {}'.format(hash_bytes))

    #EVERYTHING UP TO THIS POINT IS WORKING!   HASHBYTES IS CORRECT HERE

    dk = hashlib.pbkdf2_hmac('sha1', hash_bytes, raw_salt, iteration_count, 180)
    current_app.logger.info('dk: {}'.format(dk))

    hash = binascii.hexlify(dk)
    current_app.logger.info('hash: {}'.format(hash))
    #return (hash, salt, iterations)


    #hex = hashlib.pbkdf2_hmac('sha1', hashBytes, rawSalt, iterationCount, 180)
    #current_app.logger.info('hex: {}'.format(hex))
    
    #chef_password_hash = binascii.hexlify(hex)
    
    #dk = hashlib.pbkdf2_hmac('sha1', hashBytes, rawSalt, iterationCount)
    #dk = hashlib.pbkdf2_hmac('sha1', str.encode(hashBytes), rawSalt, iterationCount)

    #current_app.logger.info('chef_password_hash: {}'.format(chef_password_hash))

    # still need to figure these out...
    #hashBytes = hash_pbkdf2("sha1", $hashBytes, $rawSalt, $iterationCount, self::OUTPUTHASH_BYTE_COUNT, true);
    # //algo (sha1), password (the sha512 hashbytes), the raw salt, the iteration count, 
    # length of output string, raw_data (true outputs raw binary data - false does lowercase hexits)
    #return base64_encode($hashBytes);

    #base 64 encode dk
    theret = base64.b64encode(dk)
    #theret = chef_password_hash

    return theret




def base62_encode(num):
    """Encode a positive number in Base X

    Arguments:
    - `num`: The number to encode
    """

    alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    if num == 0:
        return alphabet[0]
    arr = []
    base = len(alphabet)
    while num:
        num, rem = divmod(num, base)
        arr.append(alphabet[rem])
    arr.reverse()
    return ''.join(arr)

def base62_decode(string):
    """Decode a Base X encoded string into the number

    Arguments:
    - `string`: The encoded string
    - `alphabet`: The alphabet to use for encoding
    """

    alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    base = len(alphabet)
    strlen = len(string)
    num = 0

    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += alphabet.index(char) * (base ** power)
        idx += 1

    return num
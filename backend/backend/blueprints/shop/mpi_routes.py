from flask import Blueprint, request
from flask_api import status, exceptions
import requests
import xml.etree.cElementTree as et
import json
from sqlalchemy.sql import text
from backend.db import db
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)

mpi_routes = Blueprint('mpi_routes', __name__)

#this is just a sample route that takes an MPI id and returns the vehicle description
@mpi_routes.route("/load_inspection/<id>", methods=["GET"])
@jwt_required
def load_inspection(id) -> dict:

    if not id:
        raise exceptions.ParseError()
        
    select_sql = """
                    SELECT *
                      FROM inspections
                    WHERE id=:id
                 """
    params = {
        'id': id
    }

    message = ""    
    result = db.engine.execute(text(select_sql), params).first()
    if not result:
        message = {"detail": "No Records"}
    else:
        message = {"detail": "Inspection ID {} Has Vehicle Description: {}".format(id,result['VehicleDescription'])}

    return message


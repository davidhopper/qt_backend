# Flask API Demo

This is a small demo application leveraging the serverless application and serverless-wsgi plugin
to allow us to build a "normal" Flask application that deploys into API Gateway and Lambda. Because
of this local development is greatly simplified.

One big thing to note is that this project has only been deployed against a non-Motoshop aws
account, so if you do a deploy the same way many endpoints will _not_ work - Motoshop dbs are
in our VPC, which these lambda functions are _not_ inside of. We'd need to deploy to an aws
account under/connected to our main aws account _or_ make the dbs reachable over the public
internet for all endpoints (eg /vehicle/*) to work. This works fine during local development
because an active vpn provides a route to the dbs.


### Overview and Notes

This app currently does not require AWS credentials in the vagrant box, just an active vpn
connection to the VPC so it can reach the db servers.

There is a collection of Postman requests for all endpoints in the Postman Team Library called
flask_demo_api.

CORS is wide open since we're testing locally and from different ports.


#### Serverless

The serverless framework can be run with `serverless` or `sls`. serverless is a node js tool
and we're installing it _locally_ rather than globally, which is recommended but not the norm.
Installing locally ensures everyone is using the same version and makes installs easier,
however because of this we need to use the node js runner `npx` when running serverless.
For any `serverless`/`sls` command you run, simply prefix it with `npx` like so - `npx sls help`.

#### NPM Run Scripts

There are also some npm run script helpers set up:

- `npm run lint` - Runs a linter over our python code looking for issues.
- `npm test` - Runs our automated tests.
- `npm start` - Starts the local web server for development.

They will echo the actual commands they run when used, or check package.json for more info.


### Local Development

To run the demo app locally inside of vagrant follow these steps:

- `vagrant up`
- `vagrant ssh`
- `cd /vagrant`
- `workon backend`
    - activates a pre-created python virtual environment with project packages installed
- `npm start` (while in venv)
    - serves the application locally
    - this is an npm run script that calls `npx sls wsgi serve -p 5000 --host 0.0.0.0`
        - npx is launching our local install of serverless, specifically the serverless-wsgi
          plugin's serve method, which under the hood is starting werkzeug's built-in web server.
    - `ctrl+c` will stop the server 

Once running the API will be available inside the VM as well as forwarded to the host machine
on port 5000. If you have something else running on port 5000 on the host you'll need to fix
the collision to access using the default port forward. You can access the API using curl,
Postman, or a web browser - the output will be tailored to your platform.

Any changes you make to the code on your host machine will be hot reloaded inside the vm, so
there is no need to stop and restart the server to see your changes reflected.


### Debugging

Local debugging can be accomplished in PyCharm in a few steps:

1) Configure PyCharm to run python3 inside the VM 

- Settings > Project > Project Interpreter > Add Remote
    - Type: Vagrant
    - Vagrant Instance Folder: backend/backend
    - Python interpreter path: /home/vagrant/.virtualenvs/backend/bin/python3

This should automatically set the local/remote file paths.

2) Set up the Debug Configuration in PyCharm
- Run > Edit Configurations
    - Click + and select Python
    - Script Path: /home/.../qt_backend/backend/backend/run.py
    - Environment Variables: add 'ENV' == 'dev'
    - Python Interpreter: It should auto-select the remote venv, if not select it


### Project Info

Our backend backend directory holds config files for vagrant, serverless and npm. The
backend subdirectory holds the Python package that makes up our Flask application.
The client folder holds a very simple example API frontend that can communicate with the
API; this is synced to s3 by the serverless-s3bucket-sync plugin (still testing this).

The backend folder that contains our python code is set up using the Flask
[Application Factory](http://flask.pocoo.org/docs/0.12/patterns/appfactories) pattern and
uses Flask [blueprints](http://flask.pocoo.org/docs/0.12/blueprints) to modularize the
code. Blueprints aren't strictly necessary as we won't be distributing these applications,
however it may be helpful.

`__init__.py` gives us a method to create our Flask app, which is used in `run.py` to
actually instantiate the app. `run.py` is the main entry point to the application - 
the serverless-wsgi plugin grabs the app variable itself, while the debugger uses the
__main__ dunder to run the app.


### TODO

- replace flask-api with an alternative as the project is in 'maintenance mode'
    - pyeve/eve, noirbizarre/flask-restplus, flask-restful/flask-restful
- look at tox for additional testing options
- handling domain names, base paths, etc
- local sqlite db for unit tests
- db migrations (flask-migrate, longer term...)
- sqlalchemy orm example


### Vagrant Box Info

This box is based on CentOS and includes the following software:

- [python 3](https://www.python.org)
- [virtualenvwrapper](https://bitbucket.org/virtualenvwrapper/virtualenvwrapper)
- [nvm](https://github.com/creationix/nvm)
- [SAM Local](https://github.com/awslabs/aws-sam-local)
- [docker](https://www.docker.com)

See bootstrap.sh (the vagrant provisioner file) for a full list of packages and base system
changes. See the README in the root of the repo for more info on Vagrant.
